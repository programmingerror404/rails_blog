class ApplicationController < ActionController::Base

	layout 'author'
rescue_from ActionController::RoutingError do |exception|
 logger.error 'Routing error occurred'
 render plain: '404 Not found', status: 404 
end
end
