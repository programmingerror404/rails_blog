class Post < ApplicationRecord
	extend FriendlyId
  		friendly_id :title, use: :slugged

      belongs_to :author , :optional => true
  	scope :most_recent, ->{order(id: :desc)}
  	validates :title, presence: true,uniqueness: true
  	validates :body, presence: true
  	def should_generate_new_friendly_id?
  		title_changed?
  	end

  	def day_published 
  		"Published On #{created_at.strftime('%-d %-b , %Y')}" 
  	end

  	def self.search(search)
	    where('title LIKE ?', "%#{search}%").order('id DESC')
	end

end
