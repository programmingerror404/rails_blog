class ApplicationMailer < ActionMailer::Base
  default from: 'deepanshu@mindshaft.co'
  layout 'mailer'
end
